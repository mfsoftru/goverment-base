#UseHook
#NoEnv
#SingleInstance force
#Include %A_ScriptDir%
SetWorkingDir %A_ScriptDir%

;#Include %A_ScriptDir%\System\VersionChecker.ahk
;#Include %A_ScriptDir%\System\Updater.ahk
;#Include %A_ScriptDir%\System\ConfigReader.ahk

GovBase := {}
GovBase["Modules"] := []
GovBase["Settings"] := []
GovBase["System"] := []

Loop, Files, %A_ScriptDir%\Modules\*.*, D
{
  GovBase["Modules"].Insert(A_LoopFileName)
}

Loop, Files, %A_ScriptDir%\Settings\*.*, D
{
  GovBase["Settings"].Insert(A_LoopFileName)
}

Loop, Files, %A_ScriptDir%\System\*.*, D
{
  GovBase["System"].Insert(A_LoopFileName)
}